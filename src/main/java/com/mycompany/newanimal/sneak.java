/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newanimal;

/**
 *
 * @author User
 */
public class sneak extends Reptile{
    private String name;
    public sneak(String name) {
        super(name, 0);
        this.name=name;
    }

    @Override
    public void crawl() {
        System.out.println("sneak"+name+"crawl");
    }

    @Override
    public void eat() {
         System.out.println("sneak"+name+"eat");
    }

    @Override
    public void move() {
         System.out.println("sneak"+name+"move");
    }

    @Override
    public void speak() {
         System.out.println("sneak"+name+"speak");
    }

    @Override
    public void sleep() {
         System.out.println("sneak"+name+"sleep");
    }
    
}
