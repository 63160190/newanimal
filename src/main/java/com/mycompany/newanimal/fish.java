/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newanimal;

/**
 *
 * @author User
 */
public class fish extends AquaticAnimal{
    private String name;
    public fish(String name) {
        super(name);
    }

    @Override
    public void swim() {
        System.out.println("fish"+name+"swim");
    }

    @Override
    public void eat() {
        System.out.println("fish"+name+"eat");
    }

    @Override
    public void move() {
        System.out.println("fish"+name+"move");
    }

    @Override
    public void speak() {
       System.out.println("fish"+name+"speak");
    }

    @Override
    public void sleep() {
       System.out.println("fish"+name+"sleep");
    }
}   