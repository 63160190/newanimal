/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newanimal;

/**
 *
 * @author User
 */
public class Human extends LandAnimal{
    private String nickname;
    public Human(String name) {
        super("Human",2);
           this.nickname=nickname;
    }

   
    @Override
    public void run() {
        System.out.println("Human:"+nickname+"run");
    }

    @Override
    public void eat() {
        System.out.println("Human:"+nickname+"eat");
    }

    @Override
    public void move() {
        System.out.println("Human:"+nickname+"move");
    }

    @Override
    public void speak() {
        System.out.println("Human:"+nickname+"speak");
    }

    @Override
    public void sleep() {
        System.out.println("Human:"+nickname+"sleep");
    }
    
}
