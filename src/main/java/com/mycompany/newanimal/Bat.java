/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newanimal;

/**
 *
 * @author User
 */
public class Bat extends Poltry{
    private String name;
    public Bat(String name) {
        super(name, 2);
        this.name=name;
    }

    @Override
    public void fly() {
        System.out.println("Bat"+name+"fly");
    }

    @Override
    public void eat() {
        System.out.println("Bat"+name+"eat");
    }

    @Override
    public void move() {
        System.out.println("Bat"+name+"move");
    }

    @Override
    public void speak() {
        System.out.println("Bat"+name+"move");
    }

    @Override
    public void sleep() {
        System.out.println("Bat"+name+"move");
    }
    
}
