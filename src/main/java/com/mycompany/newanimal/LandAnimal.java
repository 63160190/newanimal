/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newanimal;

/**
 *
 * @author User
 */
public abstract class LandAnimal extends Animal{
    public LandAnimal(String name,int numOfleg){
        super(name,numOfleg);
    }
    public abstract void  run();
}
