/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newanimal;

/**
 *
 * @author User
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1=new Human("Takumi");
        h1.eat();
        h1.move();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animal?"+(h1 instanceof Animal));
        System.out.println("h1 is land animal?"+(h1 instanceof LandAnimal));
        System.out.println("*************************************************");
        
        Dog d1=new Dog("Tu");
        d1.eat();
        d1.move();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is animal?"+(d1 instanceof Animal));
        System.out.println("d1 is land animal?"+(d1 instanceof LandAnimal));
        System.out.println("*************************************************");
        
        Cat c1=new Cat("City");
        c1.eat();
        c1.move();
        c1.run();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal?"+(c1 instanceof Animal));
        System.out.println("c1 is land animal?"+(c1 instanceof LandAnimal));
        System.out.println("*************************************************");
        
        fish f1=new fish("Benz");
        f1.swim();
        f1.eat();
        f1.move();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animal?"+(f1 instanceof Animal));
        System.out.println("f1 is AquaticAnimal?"+(f1 instanceof AquaticAnimal));
        System.out.println("*************************************************");
        
        Crab c2=new Crab("Bmw");
        c2.swim();
        c2.eat();
        c2.move();
        c2.speak();
        c2.sleep();
        System.out.println("c2 is animal?"+(c2 instanceof Animal));
        System.out.println("c2 is AquaticAnimal?"+(c2 instanceof AquaticAnimal));
        System.out.println("*************************************************");
        
        Bat b1=new Bat("GTR");
        b1.fly();
        b1.eat();
        b1.move();
        b1.speak();
        b1.sleep();
        System.out.println("b1 is animal?"+(b1 instanceof Animal));
        System.out.println("b1 is AquaticAnimal?"+(b1 instanceof Poltry));
        System.out.println("*************************************************");
        
        Bird b2=new Bird("GTR");
        b2.fly();
        b2.eat();
        b2.move();
        b2.speak();
        b2.sleep();
        System.out.println("b2 is animal?"+(b2 instanceof Animal));
        System.out.println("b2 is AquaticAnimal?"+(b2 instanceof Poltry));
        System.out.println("*************************************************");
        
        sneak s1=new sneak("cobra");
        s1.crawl();
        s1.eat();
        s1.move();
        s1.speak();
        s1.sleep();
        System.out.println("s1 is animal?"+(s1 instanceof Animal));
        System.out.println("s1 is AquaticAnimal?"+(s1 instanceof Reptile));
        System.out.println("*************************************************");
        
        Crocodile cc1=new Crocodile("Amg");
        cc1.crawl();
        cc1.eat();
        cc1.move();
        cc1.speak();
        cc1.sleep();
        System.out.println("c1 is animal?"+(cc1 instanceof Animal));
        System.out.println("c1 is AquaticAnimal?"+(cc1 instanceof Reptile));
        System.out.println("*************************************************");
        
        worm w1=new worm("Evo");
        w1.crawl();
        w1.eat();
        w1.move();
        w1.speak();
        w1.sleep();
        System.out.println("w1 is animal?"+(w1 instanceof Animal));
        System.out.println("w1 is AquaticAnimal?"+(w1 instanceof Reptile));
        System.out.println("*************************************************");
        
       }  
}
