/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newanimal;

/**
 *
 * @author User
 */
public class Crocodile extends Reptile{
    private String name;
    public Crocodile(String name) {
        super(name,4);
        this.name=name;
    }

    @Override
    public void crawl() {
       System.out.println("Crocodile"+name+"crawl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile"+name+"eat");
    }

    @Override
    public void move() {
        System.out.println("Crocodile"+name+"move");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile"+name+"speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile"+name+"sleep");
    }
    
}
