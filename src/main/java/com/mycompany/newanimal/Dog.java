/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newanimal;

/**
 *
 * @author User
 */
public class Dog extends LandAnimal{
    private String name;
    public Dog(String name) {
        super(name,4);
        this.name=name;
    }

   @Override
    public void run() {
        System.out.println("Human:"+name+"run");
    }

    @Override
    public void eat() {
        System.out.println("Human:"+name+"eat");
    }

    @Override
    public void move() {
        System.out.println("Human:"+name+"move");
    }

    @Override
    public void speak() {
        System.out.println("Human:"+name+"speak");
    }

    @Override
    public void sleep() {
        System.out.println("Human:"+name+"sleep");
    }
    
}