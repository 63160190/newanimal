/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newanimal;

/**
 *
 * @author User
 */
public abstract class Animal {
    String name;
    private int numberOfleg;

    public Animal(String name,int numberOfleg) {
        this.name = name;
        this.numberOfleg=numberOfleg;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfleg() {
        return numberOfleg;
    }

    public void setNumberOfleg(int numberOfleg) {
        this.numberOfleg = numberOfleg;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", numberOfleg=" + numberOfleg + '}';
    }
    
    public abstract void eat();
    public abstract void move();    
    public abstract void speak();
    public abstract void sleep();
}
